package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import noobbot.adapter.MessageAdapter;
import noobbot.models.CarData;
import noobbot.models.CarPosition;
import noobbot.models.Lane;
import noobbot.models.Piece;
import noobbot.models.Race;

// 50 ms per 3 tick on test server
// y=0.0659402 x+0.0654979 x^2-0.000441085 x^3 for throttle 0.655
public class Main {
    public static void main(String... args) throws IOException {
       // args = new String[]{"hakkinen.helloworldopen.com", "8091", "Spear", "+daR6yBDeYbeaQ"};
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinRace(botName, botKey, null, 1));
    }

    final GsonBuilder builder = new GsonBuilder();

    private PrintWriter writer;
    private Race race;
    ArrayList<CarData> carPositions = new ArrayList<CarData>();
    int carLength = 40;
    double MAX_ANGLE = 60;
    double CONSTANT_THROTTLE = 0.7;


    boolean switchedOnThisLane = false;
    boolean urgentDeacc = false;

    public Main(final BufferedReader reader, final PrintWriter writer, final JoinRace joinRace) throws IOException {
        this.writer = writer;
        builder.registerTypeAdapter(InMsgWrapper.class, new MessageAdapter());
        final Gson gson = builder.create();
        String line = null;

        send(joinRace);
        System.out.println("Constant throttle " + CONSTANT_THROTTLE);
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            final InMsgWrapper msgFromServer = gson.fromJson(line, InMsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                SendMsg outMsg = addCarPosition((CarPosition[]) msgFromServer.data, msgFromServer.gameTick);
//                double throttle=CONSTANT_THROTTLE;
//                System.out.println("Sending throttle:" + throttle);

//                send(new Throttle(throttle));
                send(outMsg);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("joinRace")) {
                System.out.println("Joined Race");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                initData((Race) msgFromServer.data);
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            }
              else if (msgFromServer.msgType.equals("turboAvailable")){
                send(new TurboOut());
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        System.out.println("Sending message:" + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }

    private void initData(Race race) {
        this.race = race;
    }


    private SendMsg addCarPosition(CarPosition[] allData, int gameTick) {
        CarPosition data = allData[0];
        int length = carPositions.size();
        String requireSwitch = null;
        CarData prev1 = null, prev2 = null;
        CarData curr = new CarData();
        if (length > 0)
            prev1 = carPositions.get(length - 1);
        if (length > 1)
            prev2 = carPositions.get(length - 2);
        curr.gameTick = gameTick;
        curr.angle = data.angle;
        curr.pieceIdx = data.piecePosition.pieceIndex;
        curr.inPieceDistance = data.piecePosition.inPieceDistance;

        Lane currentLane = data.piecePosition.lane;
        Piece currentPiece = race.track.pieces[curr.pieceIdx];

        if (prev1 != null) {
            curr.angleVelocity = curr.angle - prev1.angle;
            if (curr.pieceIdx == prev1.pieceIdx) {
                curr.velocity = curr.inPieceDistance - prev1.inPieceDistance;

            } else {
                switchedOnThisLane = false;
                double prevPieceDistance = race.track.pieces[prev1.pieceIdx].getLength(race.track.lanes[currentLane.startLaneIndex].distanceFromCenter) - prev1.inPieceDistance;
                curr.velocity = prevPieceDistance + curr.inPieceDistance;

            }
            if (prev2 != null) {
                curr.acceleration = curr.velocity - prev1.velocity;
                curr.angleAcc = curr.angleVelocity - prev1.angleVelocity;
            }
        }
        carPositions.add(curr);
        System.out.println(curr);
        int nextCurveId = race.track.getNextCurve(curr.pieceIdx);

        if (nextCurveId >= 0) {
            Piece nextCurved = race.track.pieces[nextCurveId];
            double anglePerDistance = nextCurved.getAnglePerDistanceUnit();
            anglePerDistance/=2; // magic number
            double ticksBeforeCrash=(MAX_ANGLE - Math.abs(curr.angle))/anglePerDistance;
            //double requiredVelocity = 2.15 / anglePerDistance; //magic number 4.5...
            double requiredVelocity=Math.sqrt(0.43*race.track.pieces[nextCurveId].getRadius(race.track.lanes[currentLane.startLaneIndex].distanceFromCenter));
            double distanceTillCurved = race.track.getDistanceTillPiece(curr.pieceIdx, curr.inPieceDistance, nextCurveId, race.track.lanes[currentLane.startLaneIndex].distanceFromCenter);
            double velocityDiff = curr.velocity - requiredVelocity;
            if (velocityDiff > 0) {
                if ((velocityDiff / 0.15 > distanceTillCurved / curr.velocity) || nextCurveId==curr.pieceIdx) {

                    System.out.println("Ticks till next curve :" + distanceTillCurved / curr.velocity);
                    System.out.println("Required velocity for next curve: " + requiredVelocity + ". Slowing down.");
                   return new Throttle(0);
                }
            }


            if (nextCurved.isLeftTurn()) //leftmost lane optimal
            {
                if (currentLane.startLaneIndex > 0)
                    requireSwitch = "Left";
            } else //rightMost is optimal
            {
                if (currentLane.startLaneIndex < race.track.lanes.length - 1) {
                    System.out.println("Current index:" + currentLane.startLaneIndex + ". Max index " + (race.track.lanes.length - 1));
                    requireSwitch = "Right";
                }
            }
        }

        double supposedTicksToCrashAngle = 0;
        double supposedTicksToCrashMaxAngle = Double.NaN;
        if (curr.angleVelocity != 0.0)
            supposedTicksToCrashMaxAngle = (MAX_ANGLE - Math.abs(curr.angle)) / Math.abs(curr.angleVelocity);
        // Dx= velocity*X+acceleration*X*X/2 where x is ticks
        double supposedTicksToCrashMinAngle = solveQuadratic(Math.abs(curr.angleAcc) / 2, Math.abs(curr.angleVelocity), Math.abs(curr.angle) - MAX_ANGLE);

        if (Double.isNaN(supposedTicksToCrashMinAngle)) {
            supposedTicksToCrashAngle = supposedTicksToCrashMaxAngle;
        } else
            supposedTicksToCrashAngle = supposedTicksToCrashMinAngle;
        if (supposedTicksToCrashAngle < 10) {
            System.out.println("Supposed ticks to crash Min: " + supposedTicksToCrashMinAngle);
            System.out.println("Supposed ticks to crash Max: " + supposedTicksToCrashMaxAngle);
            if (Math.abs(curr.angle) < Math.abs(prev1.angle)) {
                System.out.println("Threat gone. Angle decreasing");
//                if (race.track.pieces[curr.pieceIdx].isStraight())
                    return new Throttle(1);
 //               else
 //                   return new Throttle(curr.velocity / 10);
            } else {
                urgentDeacc = true;
            }
        }


        if (prev1 != null && requireSwitch != null && !switchedOnThisLane) {
            switchedOnThisLane = true;
            return new SwitchLane(requireSwitch);
        }


        if (urgentDeacc) {
            urgentDeacc = false;
            return new Throttle(0);
        }
        return new Throttle(1);
    }

    // Solve a*x*x+b*x+c equation.
    public static double solveQuadratic(double a, double b, double c) {
        if (a == 0.0)
            return Double.NaN;
        double determinate = b * b - 4 * a * c;
        if (determinate < 0)
            return Double.NaN;

        return (-b + Math.sqrt(determinate)) / 2 / a;

    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new InMsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class JoinRace extends SendMsg {
    public JoinData joinData;

    public JoinRace(String name, String key, String mapName, int carCount) {
        joinData = new JoinData();

        joinData.trackName = mapName;

        joinData.carCount = carCount;

        joinData.botId = new BotId();

        joinData.botId.key = key;

        joinData.botId.name = name;

    }

    protected String msgType() {

        return "joinRace";

    }

    protected Object msgData() {

        return this.joinData;

    }
}

class BotId {
    public String name;

    public String key;
}

class JoinData {
    public BotId botId;

    public String trackName;

    public int carCount;
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class TurboOut extends SendMsg {
    private String value ="Rock'n'rollin!";

    public TurboOut() {

    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
//package noobbot;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.net.Socket;
//
//import com.google.gson.Gson;
//
//public class Main {
//    public static void main(String... args) throws IOException {
//        args = new String[]{"senna.helloworldopen.com", "8091", "Spear", "+daR6yBDeYbeaQ"};
//        String host = args[0];
//        int port = Integer.parseInt(args[1]);
//        String botName = args[2];
//        String botKey = args[3];
//
//        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
//
//        final Socket socket = new Socket(host, port);
//        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
//
//        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
//
//        new Main(reader, writer, new Join(botName, botKey));
//    }
//
//    final Gson gson = new Gson();
//    private PrintWriter writer;
//
//    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
//        this.writer = writer;
//        String line = null;
//
//        send(join);
//
//        while((line = reader.readLine()) != null) {
//            System.out.println(line);
//            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//            if (msgFromServer.msgType.equals("carPositions")) {
//                send(new Throttle(0.5));
//            } else if (msgFromServer.msgType.equals("join")) {
//                System.out.println("Joined");
//            } else if (msgFromServer.msgType.equals("gameInit")) {
//                System.out.println("Race init");
//            } else if (msgFromServer.msgType.equals("gameEnd")) {
//                System.out.println("Race end");
//            } else if (msgFromServer.msgType.equals("gameStart")) {
//                System.out.println("Race start");
//            } else {
//                send(new Ping());
//            }
//        }
//    }
//
//    private void send(final SendMsg msg) {
//        writer.println(msg.toJson());
//        writer.flush();
//    }
//}
//
//abstract class SendMsg {
//    public String toJson() {
//        return new Gson().toJson(new MsgWrapper(this));
//    }
//
//    protected Object msgData() {
//        return this;
//    }
//
//    protected abstract String msgType();
//}
//
//class MsgWrapper {
//    public final String msgType;
//    public final Object data;
//
//    MsgWrapper(final String msgType, final Object data) {
//        this.msgType = msgType;
//        this.data = data;
//    }
//
//    public MsgWrapper(final SendMsg sendMsg) {
//        this(sendMsg.msgType(), sendMsg.msgData());
//    }
//}
//
//class Join extends SendMsg {
//    public final String name;
//    public final String key;
//
//    Join(final String name, final String key) {
//        this.name = name;
//        this.key = key;
//    }
//
//    @Override
//    protected String msgType() {
//        return "join";
//    }
//}
//
//class Ping extends SendMsg {
//    @Override
//    protected String msgType() {
//        return "ping";
//    }
//}
//
//class Throttle extends SendMsg {
//    private double value;
//
//    public Throttle(double value) {
//        this.value = value;
//    }
//
//    @Override
//    protected Object msgData() {
//        return value;
//    }
//
//    @Override
//    protected String msgType() {
//        return "throttle";
//    }
//}